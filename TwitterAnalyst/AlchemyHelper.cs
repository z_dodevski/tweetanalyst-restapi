﻿using DomainModels.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace TwitterAnalyst
{
    public class AlchemyHelper
    {
        private static string _alchemyApiKey = "7e4177f52155ca118a620e619e4f083519c76f78";

        #region Sentiment

        public static List<AlchemySentiment> GetSentimentFromText(List<string> phrases)
        {
            List<AlchemySentiment> sentiments = new List<AlchemySentiment>();

            foreach (string phrase in phrases)
                sentiments.Add(GetSentimentFromText(phrase));

            return sentiments;
        }

        public static AlchemySentiment GetSentimentFromText(string text)
        {
            AlchemySentiment sentiment = new AlchemySentiment();

            string url = "http://gateway-a.watsonplatform.net/calls/text/TextGetTextSentiment?apikey=" + _alchemyApiKey
                + "&text=" + text.Replace("#", "")
                + "&outputMode=json";


            WebClient wc = new WebClient();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            try
            {
                wc.Encoding = Encoding.UTF8;
                string rawJson = wc.DownloadString(url);
                sentiment = JsonConvert.DeserializeObject<AlchemySentiment>(rawJson);


                return sentiment;
            }
            catch (Exception err)
            {
                return sentiment;
            }
        }

        public static AlchemySentiment GetSentimentFromTextPost(string text)
        {
            AlchemySentiment sentiment = new AlchemySentiment();

            string url = "http://gateway-a.watsonplatform.net/calls/text/TextGetTextSentiment";

            string parameters = "apikey=" + _alchemyApiKey + "&text=" + text.Replace("#", "") + "&outputMode=json";

            WebClient wc = new WebClient();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            try
            {
                wc.Encoding = Encoding.UTF8;
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

                string rawJson = wc.UploadString(url, parameters);

                sentiment = JsonConvert.DeserializeObject<AlchemySentiment>(rawJson);

                return sentiment;
            }
            catch (Exception err)
            {
                return sentiment;
            }
        }

        #endregion

        #region Taxonomy
        public static List<AlchemyTaxonomy> GetTaxonomyFromText(List<string> phrases)
        {
            List<AlchemyTaxonomy> taxonomies = new List<AlchemyTaxonomy>();

            foreach (var phrase in phrases)
                taxonomies.Add(GetTaxonomyFromText(phrase));

            return taxonomies;
        }

        public static AlchemyTaxonomy GetTaxonomyFromText(string text)
        {
            AlchemyTaxonomy taxonomy = new AlchemyTaxonomy();

            string url = "http://gateway-a.watsonplatform.net/calls/text/TextGetRankedTaxonomy?apikey=" + _alchemyApiKey
                + "&text=" + text.Replace("#", "")
                + "&outputMode=json";

            WebClient wc = new WebClient();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            try
            {
                wc.Encoding = Encoding.UTF8;
                string rawJson = wc.DownloadString(url);
                taxonomy = JsonConvert.DeserializeObject<AlchemyTaxonomy>(rawJson);

                taxonomy.categories = taxonomy.taxonomy.Select(x => x.label.Substring(1, x.label.Length - 1)).ToList();
                taxonomy.presence = taxonomy.taxonomy.Select(x => x.score.Value).ToList();

                return taxonomy;
            }
            catch (Exception err)
            {
                return taxonomy;
            }
        }

        public static AlchemyTaxonomy GetTaxonomyFromTextPost(string text)
        {
            AlchemyTaxonomy taxonomy = new AlchemyTaxonomy();

            string url = "http://gateway-a.watsonplatform.net/calls/text/TextGetRankedTaxonomy";

            string parameters = "apikey=" + _alchemyApiKey
                + "&text=" + text.Replace("#", "")
                + "&outputMode=json";

            WebClient wc = new WebClient();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            try
            {
                wc.Encoding = Encoding.UTF8;
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

                string rawJson = wc.UploadString(url, parameters);
                taxonomy = JsonConvert.DeserializeObject<AlchemyTaxonomy>(rawJson);

                taxonomy.categories = taxonomy.taxonomy.Select(x => x.label.Substring(1, x.label.Length - 1)).ToList();
                taxonomy.presence = taxonomy.taxonomy.Select(x => x.score.Value).ToList();

                return taxonomy;
            }
            catch (Exception err)
            {
                return taxonomy;
            }
        }

        #endregion

        #region Concepts

        public static List<AlchemyConcept> GetConceptsFromText(List<string> phrases)
        {
            List<AlchemyConcept> concepts = new List<AlchemyConcept>();

            foreach (var phrase in phrases)
                concepts.Add(GetConceptsFromText(phrase));

            return concepts;
        }

        public static AlchemyConcept GetConceptsFromText(string text)
        {
            AlchemyConcept concept = new AlchemyConcept();

            string url = "http://gateway-a.watsonplatform.net/calls/text/TextGetRankedConcepts?apikey=" + _alchemyApiKey
                + "&text=" + text.Replace("#", "")
                + "&outputMode=json";

            WebClient wc = new WebClient();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            try
            {
                wc.Encoding = Encoding.UTF8;
                string rawJson = wc.DownloadString(url);
                concept = JsonConvert.DeserializeObject<AlchemyConcept>(rawJson);

                return concept;
            }
            catch (Exception err)
            {
                return concept;
            }
        }

        public static AlchemyConcept GetConceptsFromTextPost(string text)
        {
            AlchemyConcept concept = new AlchemyConcept();

            string url = "http://gateway-a.watsonplatform.net/calls/text/TextGetRankedConcepts";
                
            string urlParameters = "apikey=" + _alchemyApiKey
                + "&text=" + text.Replace("#", "")
                + "&outputMode=json";

            WebClient wc = new WebClient();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            try
            {
                wc.Encoding = Encoding.UTF8;
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                string rawJson = wc.UploadString(url, urlParameters);

                concept = JsonConvert.DeserializeObject<AlchemyConcept>(rawJson);

                return concept;
            }
            catch (Exception err)
            {
                return concept;
            }
        }

        #endregion

        #region Entities
        public static List<AlchemyEntity> GetEntitiesFromText(List<string> phrases)
        {
            List<AlchemyEntity> entities = new List<AlchemyEntity>();

            foreach (var phrase in phrases)
                entities.Add(GetEntitiesFromText(phrase));

            return entities;

        }

        public static AlchemyEntity GetEntitiesFromText(string text)
        {
            AlchemyEntity entity = new AlchemyEntity();

            string url = "http://gateway-a.watsonplatform.net/calls/text/TextGetRankedNamedEntities?apikey=" + _alchemyApiKey
                + "&text=" + text.Replace("#", "")
                + "&outputMode=json";

            WebClient wc = new WebClient();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            try
            {
                wc.Encoding = Encoding.UTF8;
                string rawJson = wc.DownloadString(url);
                entity = JsonConvert.DeserializeObject<AlchemyEntity>(rawJson);

                return entity;
            }
            catch (Exception err)
            {
                return entity;
            }
        }

        public static AlchemyEntity GetEntitiesFromTextPost(string text)
        {
            AlchemyEntity entity = new AlchemyEntity();

            string url = "http://gateway-a.watsonplatform.net/calls/text/TextGetRankedNamedEntities";
                
            string urlParameters = "apikey=" + _alchemyApiKey
            + "&text=" + text.Replace("#", "")
            + "&outputMode=json&lang=en";

            WebClient wc = new WebClient();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            try
            {
                wc.Encoding = Encoding.UTF8;
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

                string rawJson = wc.UploadString(url, urlParameters);
                entity = JsonConvert.DeserializeObject<AlchemyEntity>(rawJson);

                return entity;
            }
            catch (Exception err)
            {
                return entity;
            }
        }

        #endregion

        #region Keyword
        public static List<AlchemyKeyword> GetKeyWordFromText(List<string> phrases)
        {
            List<AlchemyKeyword> keywords = new List<AlchemyKeyword>();

            foreach (var phrase in phrases)
                keywords.Add(GetKeyWordFromText(phrase));

            return keywords;
        }

        public static AlchemyKeyword GetKeyWordFromText(string text)
        {
            AlchemyKeyword keyword = new AlchemyKeyword();

            string url = "http://gateway-a.watsonplatform.net/calls/text/TextGetRankedKeywords?apikey=" + _alchemyApiKey
                + "&text=" + text.Replace("#", "")
                + "&outputMode=json";

            WebClient wc = new WebClient();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            try
            {
                wc.Encoding = Encoding.UTF8;
                string rawJson = wc.DownloadString(url);
                keyword = JsonConvert.DeserializeObject<AlchemyKeyword>(rawJson);
                keyword.keywords = keyword.keywords.OrderBy(x => x.relevance).ToList();
                return keyword;
            }
            catch (Exception err)
            {
                return keyword;
            }
        }
        #endregion

        #region Emotion
        public static List<AlchemyEmotion> GetEmotionFromText(List<string> phrases)
        {
            List<AlchemyEmotion> emotions = new List<AlchemyEmotion>();

            foreach (var phrase in phrases)
                emotions.Add(GetEmotionFromText(phrase));

            return emotions;
        }

        public static AlchemyEmotion GetEmotionFromText(string text)
        {
            AlchemyEmotion emotion = new AlchemyEmotion();

            string url = "http://gateway-a.watsonplatform.net/calls/text/TextGetEmotion?apikey=" + _alchemyApiKey
                + "&text=" + text.Replace("#", "")
                + "&outputMode=json&lang=en";

            WebClient wc = new WebClient();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            try
            {
                wc.Encoding = Encoding.UTF8;
                string rawJson = wc.DownloadString(url);
                emotion = JsonConvert.DeserializeObject<AlchemyEmotion>(rawJson);

                emotion.emotions.Add(emotion.docEmotions.anger);
                emotion.emotions.Add(emotion.docEmotions.disgust);
                emotion.emotions.Add(emotion.docEmotions.fear);
                emotion.emotions.Add(emotion.docEmotions.joy);
                emotion.emotions.Add(emotion.docEmotions.sadness);

                return emotion;
            }
            catch (Exception err)
            {
                return emotion;
            }
        }

        public static AlchemyEmotion GetEmotionFromTextPost(string text)
        {
            AlchemyEmotion emotion = new AlchemyEmotion();

            string url = "http://gateway-a.watsonplatform.net/calls/text/TextGetEmotion";
            string parameters = "apikey=" + _alchemyApiKey + "&text=" + text.Replace("#", "") + "&outputMode=json&lang=en";

            WebClient wc = new WebClient();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            try
            {
                wc.Encoding = Encoding.UTF8;
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

                string rawJson = wc.UploadString(url, parameters);


                emotion = JsonConvert.DeserializeObject<AlchemyEmotion>(rawJson);

                emotion.emotions.Add(emotion.docEmotions.anger);
                emotion.emotions.Add(emotion.docEmotions.disgust);
                emotion.emotions.Add(emotion.docEmotions.fear);
                emotion.emotions.Add(emotion.docEmotions.joy);
                emotion.emotions.Add(emotion.docEmotions.sadness);

                return emotion;
            }
            catch (Exception err)
            {
                return emotion;
            }
        }

        #endregion

    }

}