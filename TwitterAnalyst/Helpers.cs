﻿using DomainModels.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Script.Serialization;
using TwitterAnalyst.OAuth;

namespace TwitterAnalyst
{
    public class Helpers
    {
        public async Task<Object> SendRequestToTwitterAPI (string url, Object type)
        {
            HttpClient client = new HttpClient(new OAuthMessageHandler(new HttpClientHandler()));
            HttpResponseMessage response = new HttpResponseMessage();
            response = await client.GetAsync(url);

            string jsonString = string.Empty;
            if (response.IsSuccessStatusCode)
            {
                jsonString = await response.Content.ReadAsStringAsync();
                JavaScriptSerializer ser = new JavaScriptSerializer();

                Type objectType = type.GetType();

                return ser.Deserialize(jsonString, objectType); 
            }
            return null;
        }

        public async Task<List<Tweet>> GetTweets(string url)
        {
            List<Tweet> tweets = new List<Tweet>();
            tweets = await SendRequestToTwitterAPI(url, tweets) as List<Tweet>;
            return tweets;
        }

        public async Task<List<User>> GetUsers(string url)
        {
            List<User> users = new List<User>();
            users = await SendRequestToTwitterAPI(url, users) as List<User>;
            return users;
        }           

        public async Task<Tweet> GetTweet(string url)
        {
            Tweet tweet = new Tweet();
            tweet = await SendRequestToTwitterAPI(url, tweet) as Tweet;
            return tweet;
        }

        public async Task<User> GetAdditionalStatisticsForUser(User user)
        {
            string url = "https://api.twitter.com/1.1/statuses/user_timeline.json?user_id=" + user.id + "&count=100&include_rts=false";
            string urlSession = url + "&max_id=";
            
            List<Tweet> tweets = new List<Tweet>();
            List<Tweet> tweetsSession = await GetTweets(url);

            int i = 3;
            tweets.AddRange(tweetsSession);


            while (tweetsSession.Any() && i > 0)
            {
                long minId = tweets.Min(x => x.id).Value;

                tweetsSession = new List<Tweet>();
                tweetsSession = await GetTweets(urlSession + minId);

                tweets.AddRange(tweetsSession);
                i--;
            }

            if(tweets.Any())
            {
                user.tweets = tweets;
                user.userMentions = MostMentionedUsers(tweets);
                user.hashTags = MostUsedHashtags(tweets);
                user.mostLikedTweets = MostLikedTweets(tweets);
               
            }
            return user;
        }

        public Tweet GetAdditionalStatisticsForTweet(Tweet tweet)
        {
            tweet.translatedText = YandexHelper.Translate(tweet.text, "mk", "en");
            tweet.extractedEntities = DandelionHelper.EntityExtraction(tweet.translatedText);
            tweet.sentimentResult = DandelionHelper.DefineSentiment(tweet.translatedText);           
            tweet.classificationChartData = DandelionHelper.CreateClassificationChart(tweet.translatedText);

            #region Alchemy API
            tweet.alchemyConcept = AlchemyHelper.GetConceptsFromText(tweet.translatedText);
            tweet.alchemyEmotion = AlchemyHelper.GetEmotionFromText(tweet.translatedText);
            tweet.alchemyEntity = AlchemyHelper.GetEntitiesFromText(tweet.translatedText);
            tweet.alchemyKeyword = AlchemyHelper.GetKeyWordFromText(tweet.translatedText);
            tweet.alchemySentiment = AlchemyHelper.GetSentimentFromText(tweet.translatedText);
            tweet.alchemyTaxonomy = AlchemyHelper.GetTaxonomyFromText(tweet.translatedText);            

            #endregion

            return tweet; 
        }

        public List<UserMentions> MostMentionedUsers(List<Tweet> tweets)
        {
            List<UserMentions> userMentions = tweets.Select(x => x.entities).SelectMany(x => x.user_mentions).ToList();
            List<UserMentions> topUserMentions = new List<UserMentions>();

            List<long> maxRepeatedUsers = userMentions.GroupBy(q => q.id)
                                    .OrderByDescending(gp => gp.Count())
                                    .Select(x => x.Key)
                                    .Take(5).ToList();

            if (maxRepeatedUsers.Any())
                foreach (var userId in maxRepeatedUsers) 
                    topUserMentions.Add(userMentions.Where(x => x.id == userId).First());

            return topUserMentions;
        }

        public List<Hashtags> MostUsedHashtags(List<Tweet> tweets)
        {
            List<Hashtags> hashtags = tweets.Select(x => x.entities).SelectMany(x => x.hashtags).ToList();

            List<Hashtags> mostUsedHashtags = hashtags.GroupBy(q => q.text)
                                    .OrderByDescending(gp => gp.Count())
                                    .Take(5)
                                    .Select(x => new Hashtags(x.Key))
                                    .ToList();

            return mostUsedHashtags;
        }

        public List<Tweet> MostLikedTweets(List<Tweet> tweets)
        {
            List<Tweet> mostLikedTweets = tweets.OrderBy(x => x.favorite_count).Take(5).ToList();
            return mostLikedTweets; 
        }

        public static DateTime ParseTwitterTime(string date)
        {
            const string format = "ddd MMM dd HH:mm:ss zzzzz yyyy";
            return DateTime.ParseExact(date, format, CultureInfo.InvariantCulture);
        }

        public ReportTextModel FillReportData(List<Tweet> tweets)
        {
            ReportTextModel model = new ReportTextModel();
            int year = 2016;
            if (tweets.Any())
            {
                try
                {
                    foreach (var tweet in tweets)
                        tweet.dateCreated = Helpers.ParseTwitterTime(tweet.created_at);

                    List<int> months = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
                    List<MontlyReportModel> monthlyReportModels = new List<MontlyReportModel>();

                    int year2015 = tweets.Count(x => x.dateCreated.HasValue && x.dateCreated.Value.Year == 2015);
                    int year2016 = tweets.Count(x => x.dateCreated.HasValue && x.dateCreated.Value.Year == 2016);



                    year = year2015 >= year2016 ? 2015 : 2016;

                    if (year2015 == 0 && year2016 == 0)
                        year = tweets.OrderByDescending(x => x.dateCreated).First().dateCreated.Value.Year;

                    model.year = year;

                    foreach (var month in months)
                    {
                        MontlyReportModel reportModel = new MontlyReportModel() { month = month, year = year };

                        List<string> tweetPhrases = tweets.Where(x => x.dateCreated.HasValue && x.dateCreated.Value.Year == year && x.dateCreated.Value.Month == month)
                            .Select(x => x.text.Replace("&", "")).ToList();

                        string concatenatedTweets = string.Join(System.Environment.NewLine, tweetPhrases);

                        int length = concatenatedTweets.Length;
                        int maxWidth = 10000;
                        int processedText = 0;

                        string translation = string.Empty;

                        while (processedText < length)
                        {
                            if (processedText + maxWidth >= length)
                                maxWidth = length - processedText;

                            string subString = concatenatedTweets.Substring(processedText, maxWidth);
                            string tweetPhrasesTranslated = YandexHelper.TranslatePost(subString, "mk", "en");

                            translation += tweetPhrasesTranslated;
                            processedText += maxWidth;
                        }
                        reportModel.text = translation;

                        monthlyReportModels.Add(reportModel);
                    }

                    model.columnChartModel = ReportHelper.FillColumnChartModel(monthlyReportModels);
                    model.emotionsChartModel = ReportHelper.FillEmotionsChartModel(monthlyReportModels);
                    model.dailyTweetsChartModel = ReportHelper.DailyTweets(tweets, year);
                    model.taxonomyChartModel = ReportHelper.FillTaxonomyChartModel(monthlyReportModels);
                }
                catch (Exception e)
                {
                    string error = e.Message;
                }

               
            }


            return model;
        }
    }
}