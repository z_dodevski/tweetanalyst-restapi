﻿using DomainModels;
using DomainModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace TwitterAnalyst.Controllers
{
    public class GroupController : ApiController
    {
        public Helpers helpers = new Helpers();

        [HttpGet]
        public async Task<UserGroup> GetGroupTimelineTweets(string userIds)
        {
            List<string> ids = userIds.Split(',').ToList();
            List<User> users = new List<User>();

            foreach (var id in ids)
            {
                User user = new User();
                string url = "https://api.twitter.com/1.1/users/show.json?user_id=" + id;
                user = await helpers.SendRequestToTwitterAPI(url, user) as User;
                if (user != null)
                {
                    user.profile_image_url = user.profile_image_url.Replace("_normal", "");
                    user = await helpers.GetAdditionalStatisticsForUser(user);
                }
                users.Add(user);
            }


            UserGroup group = new UserGroup();

            group.users = users;
            group.tweets = users.SelectMany(x => x.tweets).ToList();
            group.tweets.ForEach(x => x.dateCreated = Helpers.ParseTwitterTime(x.created_at));
            group.tweets = group.tweets.OrderByDescending(x => x.dateCreated).ToList();
            group.mostLikedTweets = users.SelectMany(x => x.mostLikedTweets).OrderBy(x => x.id).ToList(); 
            group.hashTags = users.SelectMany(x => x.hashTags).ToList(); 
            group.userMentions = users.SelectMany(x => x.userMentions).ToList(); 
            group.retweeters = users.SelectMany(x => x.retweeters).ToList();

            group.entities = users.SelectMany(x => x.entities).ToList();
            group.favourites_count = users.Where(x => x.favourites_count.HasValue).Sum(y => y.favourites_count);
            group.followers_count = users.Where(x => x.followers_count.HasValue).Sum(y => y.followers_count);
            group.friends_count = users.Where(x => x.friends_count.HasValue).Sum(y => y.friends_count);
            group.statuses_count = users.Where(x => !string.IsNullOrWhiteSpace(x.statuses_count)).Sum(y => Int32.Parse(y.statuses_count)).ToString();
            group.statuses = group.tweets.Take(30).ToList();
            group.report = helpers.FillReportData(group.tweets);
            return group;
        }


        public async Task<List<User>> GetGroup(string userIds)
        {
            List<User> users = new List<User>();
            string url = "https://api.twitter.com/1.1/users/lookup.json?";

            url += "include_entities=true";

            if (!string.IsNullOrWhiteSpace(userIds))
                url += "&user_id=" + userIds.Replace(",", "%2C");
            users = await helpers.GetUsers(url);
            return users;
        }
    }
}

