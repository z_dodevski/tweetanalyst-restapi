﻿using BusinessLayer;
using DomainModels.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Script.Serialization;
using TwitterAnalyst.OAuth;

namespace TwitterAnalyst.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserController : ApiController
    {
        public StatusService statusService = new StatusService();
        public Helpers helpers = new Helpers();
        /// <summary>
        /// Returns a variety of information about the user specified by the required user_id or screen_name parameter.
        /// The author’s most recent Tweet will be returned inline when possible.
        /// </summary>
        /// <param name="screen_name">The screen name of the user for whom to return results for. 
        /// Either a id or screen_name is required for this method.</param>
        /// <param name="user_id">The ID of the user for whom to return results for.
        /// Either an id or screen_name is required for this method.</param>
        /// <returns>User</returns>
        [HttpGet]
        public async Task<User> Get(string screen_name = "", long? user_id = null, bool includeStatistics = true, bool generateJson = true)
        {
            User user = new User();
            string url = "https://api.twitter.com/1.1/users/show.json";

            if (!string.IsNullOrWhiteSpace(screen_name))
                url += "?screen_name=" + screen_name;

            if (user_id.HasValue)
                url += url.Contains("?") ? "&user_id=" + user_id : "?user_id=" + user_id;

            user = await helpers.SendRequestToTwitterAPI(url, user) as User;

            if (user != null)
            {
                user.profile_image_url = user.profile_image_url.Replace("_normal", "");
                if (includeStatistics)
                    user = await helpers.GetAdditionalStatisticsForUser(user);
            }

            if (generateJson)
            {
                string json = JsonConvert.SerializeObject(user, Formatting.Indented);

                string serverName = HostingEnvironment.ApplicationPhysicalPath;
                string directory = Path.Combine(serverName, "DataFolders");
                if (!Directory.Exists(directory))
                    Directory.CreateDirectory(directory);

                string filePath = Path.Combine(serverName, "DataFolders", "UserOverview_" + user.id + ".txt");
                File.WriteAllText(filePath, json, System.Text.Encoding.Unicode);

            }
            return user;
        }

        /// <summary>
        /// Returns fully-hydrated user objects for up to 100 users per request, 
        /// as specified by comma-separated values passed to the user_id and/or screen_name parameters.
        /// </summary>
        /// <param name="screen_name">A comma separated list of screen names, up to 100 are allowed in a single request. 
        /// You are strongly encouraged to use a POST for larger (up to 100 screen names) requests.</param>
        /// <param name="user_id">A comma separated list of user IDs, up to 100 are allowed in a single request. You are strongly encouraged to use a POST for larger requests.</param>
        /// <returns>User model</returns>
        public async Task<List<User>> Lookup(string screen_name = "", int? user_id = null)
        {
            string url = "https://api.twitter.com/1.1/users/lookup.json";

            if (!string.IsNullOrWhiteSpace(screen_name))
                url += "?screen_name=" + screen_name;

            if (user_id.HasValue)
                url += url.Contains("?") ? "&user_id=" + user_id : "?user_id=" + user_id;


            List<User> users = new List<User>();

            HttpClient client = new HttpClient(new OAuthMessageHandler(new HttpClientHandler()));
            HttpResponseMessage response = new HttpResponseMessage();


            response = await client.GetAsync(url);

            string jsonString = string.Empty;
            if (response.IsSuccessStatusCode)
            {
                jsonString = await response.Content.ReadAsStringAsync();
                JavaScriptSerializer ser = new JavaScriptSerializer();
                users = (List<User>)ser.Deserialize(jsonString, typeof(List<User>));
            }
            return users;
        }

        /// <summary>
        /// Provides a simple, relevance-based search interface to public user accounts on Twitter.
        /// Try querying by topical interest, full name, company name, location, or other criteria. Exact match searches are not supported.
        /// </summary>
        /// <param name="query">
        /// The search query to run against people search.
        /// </param>
        /// <param name="page">Specifies the page of results to retrieve.</param>
        /// <param name="count">The number of potential user results to retrieve per page. This value has a maximum of 20.</param>
        /// <returns>User</returns>
        [HttpGet]
        public async Task<List<User>> Search(string query, int page = 1, int count = 20)
        {
            List<User> users = new List<User>();

            string url = "https://api.twitter.com/1.1/users/search.json";

            if (!string.IsNullOrWhiteSpace(query))
                url += "?q=" + query +
                    "&page=" + page +
                    "&count=" + count;

            HttpClient client = new HttpClient(new OAuthMessageHandler(new HttpClientHandler()));
            HttpResponseMessage response = new HttpResponseMessage();

            response = await client.GetAsync(url);

            string jsonString = string.Empty;
            if (response.IsSuccessStatusCode)
            {
                jsonString = await response.Content.ReadAsStringAsync();
                JavaScriptSerializer ser = new JavaScriptSerializer();
                users = (List<User>)ser.Deserialize(jsonString, typeof(List<User>));
                users.ForEach(x => x.profile_image_url.Replace("normal", "bigger"));
            }

            return users;
        }

        public async Task<UserIDs> GetRetweetUsers(long retweetedStatusId, int count = 100)
        {
            UserIDs userIds = new UserIDs();

            string url = "https://api.twitter.com/1.1/statuses/retweeters/ids.json?id=" + retweetedStatusId +
                "&count=" + count +
                "&stringify_ids=true";

            HttpClient client = new HttpClient(new OAuthMessageHandler(new HttpClientHandler()));
            HttpResponseMessage response = new HttpResponseMessage();

            response = await client.GetAsync(url);

            string jsonString = string.Empty;
            if (response.IsSuccessStatusCode)
            {
                jsonString = await response.Content.ReadAsStringAsync();
                JavaScriptSerializer ser = new JavaScriptSerializer();
                userIds = (UserIDs)ser.Deserialize(jsonString, typeof(UserIDs));
            }

            return userIds;
        }

        [HttpGet]
        public async Task<UserComparison> CompareUsers(long firstUser_Id, long secondUser_Id)
        {
            UserComparison userComparison = new UserComparison();

            User firstUser = new User();
            User secondUser = new User();

            string firstUserUrl = "https://api.twitter.com/1.1/users/show.json?user_id=" + firstUser_Id;
            firstUser = await helpers.SendRequestToTwitterAPI(firstUserUrl, firstUser) as User;
            userComparison.firstUser = firstUser;


            string secondUserUrl = "https://api.twitter.com/1.1/users/show.json?user_id=" + secondUser_Id;
            secondUser = await helpers.SendRequestToTwitterAPI(secondUserUrl, secondUser) as User;
            userComparison.secondUser = secondUser;


            string urlFirstUserTimeline = "https://api.twitter.com/1.1/statuses/user_timeline.json?user_id=" + +firstUser_Id;
            urlFirstUserTimeline += "&include_rts=false&exclude_replies=true&count=200";

            string urlSecondUserTimeline = "https://api.twitter.com/1.1/statuses/user_timeline.json?user_id=" + +secondUser_Id;
            urlSecondUserTimeline += "&include_rts=false&exclude_replies=true&count=200";


            List<Tweet> firstUserTweets = await helpers.GetTweets(urlFirstUserTimeline);
            List<Tweet> secondUserTweets = await helpers.GetTweets(urlSecondUserTimeline);


            List<string> hashTags = firstUserTweets.SelectMany(x => x.entities.hashtags.Select(y => y.text)).ToList();
            userComparison.commonHashtags = secondUserTweets.SelectMany(x => x.entities.hashtags.Where(y => hashTags.Contains(y.text))).ToList();

            List<long> userMentionIds = firstUserTweets.SelectMany(x => x.entities.user_mentions.Select(y => y.id)).ToList();
            userComparison.commonUserMentions = secondUserTweets.SelectMany(x => x.entities.user_mentions.Where(y => userMentionIds.Contains(y.id))).ToList();

            userComparison.tweetSimilarity = DandelionHelper.CompareTweets(firstUserTweets, secondUserTweets);
            userComparison.mostSimilarTweet = userComparison.tweetSimilarity.OrderByDescending(x => x.similarityResult).FirstOrDefault();

            return userComparison;
        }

        [HttpGet]
        public async Task<TweetsSimilarity> Compare(long firstUser_Id, long secondUser_Id)
        {
            User firstUser = new User();
            User secondUser = new User();

            string firstUserUrl = "https://api.twitter.com/1.1/users/show.json?user_id=" + firstUser_Id;
            firstUser = await helpers.SendRequestToTwitterAPI(firstUserUrl, firstUser) as User;

            string secondUserUrl = "https://api.twitter.com/1.1/users/show.json?user_id=" + secondUser_Id;
            secondUser = await helpers.SendRequestToTwitterAPI(secondUserUrl, secondUser) as User;

            string urlFirstUserTimeline = "https://api.twitter.com/1.1/statuses/user_timeline.json?user_id=" + +firstUser_Id;
            urlFirstUserTimeline += "&include_rts=true&exclude_replies=true&count=500";

            string urlSecondUserTimeline = "https://api.twitter.com/1.1/statuses/user_timeline.json?user_id=" + +secondUser_Id;
            urlSecondUserTimeline += "&include_rts=true&exclude_replies=true&count=500";


            List<Tweet> firstUserTweets = await helpers.GetTweets(urlFirstUserTimeline);
            List<Tweet> secondUserTweets = await helpers.GetTweets(urlSecondUserTimeline);

            List<string> firstUserPhrases = firstUserTweets
                           .Select(x => x.text.Replace("&", "")).ToList();

            List<string> secondUserPhrases = secondUserTweets
                           .Select(x => x.text.Replace("&", "")).ToList();

            string concatenatedTweetsFirstUser = string.Join(System.Environment.NewLine, firstUserPhrases);
            string concatenatedTweetsFirstUserTranslated = YandexHelper.TranslateLongText(concatenatedTweetsFirstUser, "mk", "en");
            AlchemyEntity FirstUserEntities = AlchemyHelper.GetEntitiesFromTextPost(concatenatedTweetsFirstUserTranslated);
            AlchemyConcept FirstUserConcepts = AlchemyHelper.GetConceptsFromTextPost(concatenatedTweetsFirstUserTranslated);

            string concatenatedTweetsSecondUser = string.Join(System.Environment.NewLine, secondUserPhrases);
            string concatenatedTweetsSecondUserTranslated = YandexHelper.TranslateLongText(concatenatedTweetsSecondUser, "mk", "en");
            AlchemyEntity SecondUserEntities = AlchemyHelper.GetEntitiesFromTextPost(concatenatedTweetsSecondUserTranslated);
            AlchemyConcept SecondUserConcepts = AlchemyHelper.GetConceptsFromTextPost(concatenatedTweetsSecondUserTranslated);


            List<TextConcept> FirstUserTextConcepts = FirstUserConcepts.concepts;
            List<TextConcept> SecondUserTextConcepts = SecondUserConcepts.concepts;
            List<TextEntity> FirstUserTextEntities = FirstUserEntities.entities;
            List<TextEntity> SecondUserTextEntities = SecondUserEntities.entities; 


            List<TextConcept> textconcepts = FirstUserTextConcepts.Where(x => SecondUserTextConcepts.Select(y => y.text).Contains(x.text)).ToList();
            List<TextEntity> textentities = FirstUserTextEntities.Where(x => SecondUserTextEntities.Select(y => y.text).Contains(x.text)).ToList();

            SimilarityResult similarityResult = DandelionHelper.TextSimilarityPost(concatenatedTweetsFirstUserTranslated, concatenatedTweetsSecondUserTranslated);




            List<string> firstUserHashtags = firstUserTweets.Where(x=>x.entities != null).SelectMany(x=> x.entities.hashtags).Select(y => y.text).ToList();
            List<string> firstUserMentionedUsers = firstUserTweets.Where(x => x.entities != null && x.entities.user_mentions != null).SelectMany(x => x.entities.user_mentions).Select(y => y.name).ToList();

            List<string> secondUserHashtags = secondUserTweets.Where(x => x.entities != null).SelectMany(x => x.entities.hashtags).Select(y => y.text).ToList();
            List<string> secondUserMentionedUsers = secondUserTweets.Where(x => x.entities != null && x.entities.user_mentions != null).SelectMany(x => x.entities.user_mentions).Select(y => y.name).ToList();




            List<string> comparedHashTags = firstUserHashtags.Intersect(secondUserHashtags).ToList();
            List<string> compareMentionedUsers = firstUserMentionedUsers.Intersect(secondUserMentionedUsers).ToList();







            TweetsSimilarity tweetsSimilarity = new TweetsSimilarity();

            tweetsSimilarity.firstUser = firstUser;
            tweetsSimilarity.secondUser = secondUser;
            tweetsSimilarity.secondUser.profile_image_url = tweetsSimilarity.secondUser.profile_image_url.Replace("_normal", "");
            tweetsSimilarity.similarityResult = similarityResult;
            tweetsSimilarity.concepts = textconcepts;
            tweetsSimilarity.entities = textentities;
            tweetsSimilarity.hashtags = comparedHashTags;
            tweetsSimilarity.mentionedUsers = compareMentionedUsers;
              

            return tweetsSimilarity;
        }

    }
}
