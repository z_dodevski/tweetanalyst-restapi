﻿using DomainModels.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Script.Serialization;
using TwitterAnalyst.OAuth;

namespace TwitterAnalyst.Controllers
{
    public class TweetController : ApiController
    {
        public Helpers helpers = new Helpers();

        [HttpGet]
        public async Task<List<Tweet>> GetUserTimelineTweets(long? user_id = null, string screen_name = "", int? count = 100)
        {
            List<Tweet> tweets = new List<Tweet>();
            string url = "https://api.twitter.com/1.1/statuses/user_timeline.json";

            if (!string.IsNullOrWhiteSpace(screen_name))
                url += "?screen_name=" + screen_name;

            if (user_id.HasValue)
                url += url.Contains("?") ? "&user_id=" + user_id : "?user_id=" + user_id;

            if (count.HasValue)
                url += "&count=" + count;

            url += "&include_rts=false&exclude_replies=true";

            tweets = await helpers.GetTweets(url);           
            return tweets.Take(20).ToList();
        }
        
        [HttpGet]
        public async Task<Tweet> Get(long id, bool includeStatistics = true)
        {
            Tweet tweet = new Tweet();
            string url = "https://api.twitter.com/1.1/statuses/show.json?id=" + id;
            tweet = await helpers.GetTweet(url);

            if (includeStatistics)
                tweet = helpers.GetAdditionalStatisticsForTweet(tweet);

            string json = JsonConvert.SerializeObject(tweet, Formatting.Indented);

            string serverName = HostingEnvironment.ApplicationPhysicalPath;
            string directory = Path.Combine(serverName, "DataFolders");
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            string filePath = Path.Combine(serverName, "DataFolders", "Tweet_" + id + ".txt");
            File.WriteAllText(filePath, json, System.Text.Encoding.Unicode);

            return tweet;
        }
    }
}
