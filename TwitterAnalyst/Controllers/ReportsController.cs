﻿using DomainModels.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using TwitterAnalyst.OAuth;

namespace TwitterAnalyst.Controllers
{
    public class ReportsController : ApiController
    {
        public Helpers helpers = new Helpers();

        [HttpGet]
        public async Task<ReportTextModel> Get(long user_id)
        {
            string url = "https://api.twitter.com/1.1/statuses/user_timeline.json?user_id=" + user_id + "&count=200&include_rts=false";
            string urlSession = url + "&max_id=";

            List<Tweet> tweets = new List<Tweet>();
            List<Tweet> tweetsSession = await helpers.GetTweets(url);

            ReportTextModel model = new ReportTextModel();

            int i = 5;
            tweets.AddRange(tweetsSession);

            while (tweetsSession.Any() && i > 0)
            {
                long minId = tweets.Min(x => x.id).Value;

                tweetsSession = new List<Tweet>();
                tweetsSession = await helpers.GetTweets(urlSession + minId);

                tweets.AddRange(tweetsSession);
                i--;
            }

            if (tweets.Any())
            {
                foreach (var tweet in tweets)
                {
                    tweet.dateCreated = Helpers.ParseTwitterTime(tweet.created_at);
                }

                tweets.ForEach(x => x.dateCreated = Helpers.ParseTwitterTime(x.created_at));

                List<string> tweetPhrases = tweets.Select(x => x.text.Replace("&", "")).ToList();
                string concatenatedTweets = string.Join(System.Environment.NewLine, tweetPhrases);


                int length = concatenatedTweets.Length;

                int maxWidth = 10000;
                int processedText = 0;
                string translation = string.Empty;


                try
                {
                    while (processedText < length)
                    {
                        if (processedText + maxWidth >= length)
                            maxWidth = length - processedText;

                        string subString = concatenatedTweets.Substring(processedText, maxWidth);
                        string tweetPhrasesTranslated = YandexHelper.TranslatePost(subString, "mk", "en");

                        translation += tweetPhrasesTranslated;
                        processedText += maxWidth;
                    }
                }
                catch (Exception err)
                {
                    return null;
                }

                model.alchemySentiments = AlchemyHelper.GetSentimentFromTextPost(translation);

                model.alchemyConcepts = AlchemyHelper.GetConceptsFromText(translation);
                model.alchemyEmotions = AlchemyHelper.GetEmotionFromText(translation);
                model.alchemyEntities = AlchemyHelper.GetEntitiesFromText(translation);
                model.alchemyKeywords = AlchemyHelper.GetKeyWordFromText(translation);
                model.alchemyTaxonomys = AlchemyHelper.GetTaxonomyFromText(translation);

            }
            return model;
        }

        [HttpGet]
        public async Task<ReportTextModel> GetYearly(long user_id, int year = 2015)
        {
            string url = "https://api.twitter.com/1.1/statuses/user_timeline.json?user_id=" + user_id + "&count=200&trim_user=true";
            string urlSession = url + "&max_id=";

            List<Tweet> tweets = new List<Tweet>();

            DateTime start = DateTime.Now;
            List<Tweet> tweetsSession = await helpers.GetTweets(url);

            ReportTextModel model = new ReportTextModel();

            int i = 6;
            tweets.AddRange(tweetsSession);

            //while (tweetsSession.Any() && i > 0)
            //{
            //    long minId = tweets.Min(x => x.id).Value;

            //    tweetsSession = new List<Tweet>();
            //    tweetsSession = await helpers.GetTweets(urlSession + minId);

            //    tweets.AddRange(tweetsSession);
            //    i--;
            //}
            DateTime end = DateTime.Now;

            int seconds = end.Subtract(start).Seconds;
            model = helpers.FillReportData(tweets);
            string json = JsonConvert.SerializeObject(model, Formatting.Indented);

            string serverName = HostingEnvironment.ApplicationPhysicalPath;
            string directory = Path.Combine(serverName, "DataFolders");
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            string filePath = Path.Combine(serverName, "DataFolders", "Reports_" + user_id + ".txt");
            File.WriteAllText(filePath, json, System.Text.Encoding.Unicode);
            return model;
        }

        [HttpGet]
        public async Task<ReportModel> Analyse(long user_id)
        {
            string url = "https://api.twitter.com/1.1/statuses/user_timeline.json?user_id=" + user_id + "&count=200&include_rts=false";
            string urlSession = url + "&max_id=";

            List<Tweet> tweets = new List<Tweet>();
            List<Tweet> tweetsSession = await helpers.GetTweets(url);

            ReportModel model = new ReportModel();

            int i = 5;
            tweets.AddRange(tweetsSession);

            while (tweetsSession.Any() && i > 0)
            {
                long minId = tweets.Min(x => x.id).Value;

                tweetsSession = new List<Tweet>();
                tweetsSession = await helpers.GetTweets(urlSession + minId);

                tweets.AddRange(tweetsSession);
                i--;
            }

            if (tweets.Any())
            {
                foreach (var tweet in tweets)
                    tweet.translatedText = YandexHelper.Translate(tweet.text, "mk", "en");

                List<string> tweetPhrases = tweets.Select(x => x.translatedText).ToList();

                model.alchemyConcepts = AlchemyHelper.GetConceptsFromText(tweetPhrases);
                model.alchemyEmotions = AlchemyHelper.GetEmotionFromText(tweetPhrases);
                model.alchemyEntities = AlchemyHelper.GetEntitiesFromText(tweetPhrases);
                model.alchemyKeywords = AlchemyHelper.GetKeyWordFromText(tweetPhrases);
                model.alchemySentiments = AlchemyHelper.GetSentimentFromText(tweetPhrases);
                model.alchemyTaxonomys = AlchemyHelper.GetTaxonomyFromText(tweetPhrases);

            }
            return model;
        }

        [HttpGet]
        public string DownloadFilePath(string downloadType, long user_id, string tweet_id = "")
        {
            if (downloadType == "Overview")
                return Path.Combine("http://"+ HostingEnvironment.ApplicationHost.GetSiteName(), "DataFolders", "UserOverview_" + user_id + ".txt");

            if(downloadType == "Reports")
                return Path.Combine("http://" + HostingEnvironment.ApplicationHost.GetSiteName(), "DataFolders", "Reports_" + user_id + ".txt");
            
            if(downloadType == "Tweet")
                return Path.Combine("http://" + HostingEnvironment.ApplicationHost.GetSiteName(), "DataFolders", "Tweet_" + tweet_id + ".txt");

            
            else

                return string.Empty;

        }      
    }
}
