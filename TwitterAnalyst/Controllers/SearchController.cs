﻿using DomainModels.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using TwitterAnalyst.OAuth;

namespace TwitterAnalyst.Controllers
{
    public class SearchController : ApiController
    {
        private static string _address = "https://api.twitter.com//1.1/search/tweets.json?q=melbourne&geocode=-37.8143%2C144.963%2C1000km&count=10";

        // GET: api/Search
       
        public async Task<StatusResponse> GetTweetsByLocation(double longitude = 41.998013, double latitude = 21.427224, int radius = 10)
        {
            string address = "https://api.twitter.com/1.1/search/tweets.json?q=&geocode="
                + longitude.ToString() + "%2C" 
                + latitude.ToString() + "%2C" 
                + radius.ToString() + "km&count=10";

            HttpClient client = new HttpClient(new OAuthMessageHandler(new HttpClientHandler()));
            HttpResponseMessage response = new HttpResponseMessage();
      

            response = await client.GetAsync(address);
            
            StatusResponse statusResponse = null;
            if (response.IsSuccessStatusCode)
            {
                var jsonString = await response.Content.ReadAsByteArrayAsync();

                string encodedString = Encoding.UTF8.GetString(jsonString, 0, jsonString.Length - 1);
              
                JavaScriptSerializer ser = new JavaScriptSerializer();
                statusResponse = (StatusResponse)ser.Deserialize(encodedString, typeof(StatusResponse));
                int i = 0;

                foreach (var status in statusResponse.statuses)
                {
                    string translatedText = YandexHelper.Translate(status.text, "mk", "en");
                   statusResponse.statuses[i].text += " --- " + translatedText;
                   statusResponse.statuses[i].text += " --- " + DandelionHelper.EntityExtraction(translatedText);
                   statusResponse.statuses[i].text += " --- " + DandelionHelper.DefineSentiment(translatedText);
                   i++; 
                }

            }
            return statusResponse;
        }

        public async Task<JToken> GetPlacesByGeoQuery(string query = "Skopje")
        {
            string address = "https://api.twitter.com/1.1/geo/search.json?query=" + query;

            HttpClient client = new HttpClient(new OAuthMessageHandler(new HttpClientHandler()));
            HttpResponseMessage response = await client.GetAsync(address);
            JToken statuses = null;
            if (response.IsSuccessStatusCode)
            {
                statuses = await response.Content.ReadAsAsync<JToken>();
                return statuses;
            }
            return statuses;
        }

        public async Task<JToken> GetPlacesByGeoLocation(double longitude = 41.998013, double latitude = 21.427224)
        {
            string address = "https://api.twitter.com/1.1/geo/search.json?long=" + latitude.ToString() + "&lat=" + longitude.ToString();

            HttpClient client = new HttpClient(new OAuthMessageHandler(new HttpClientHandler()));
            HttpResponseMessage response = await client.GetAsync(address);
            JToken statuses = null;
            if (response.IsSuccessStatusCode)
            {
                statuses = await response.Content.ReadAsAsync<JToken>();
                return statuses;
            }
            return statuses;
        }

        public async Task<JToken> GetTimeline()
        {
            HttpClient client = new HttpClient(new OAuthMessageHandler(new HttpClientHandler()));
            HttpResponseMessage response = await client.GetAsync(_address);
            JToken statuses = string.Empty;

            if (response.IsSuccessStatusCode)
            {
                statuses = await response.Content.ReadAsAsync<JToken>();
                return statuses.First().ToString();
            }
            return statuses;
        }

        public async Task<JToken> GetStatuses()
        {
            HttpClient client = new HttpClient(new OAuthMessageHandler(new HttpClientHandler()));
            HttpResponseMessage response = await client.GetAsync(_address);
            JToken statuses = null;
            if (response.IsSuccessStatusCode)
            {
                statuses = await response.Content.ReadAsAsync<JToken>();
                return statuses;
            }
            return statuses;
        }

        public async Task<JToken> GetTweetsInSkopje(string language)
        {
            string address = "https://api.twitter.com/1.1/search/tweets.json?q=&geocode=41.998013%2C21.427224%2C10km&count=20";

            HttpClient client = new HttpClient(new OAuthMessageHandler(new HttpClientHandler()));
            HttpResponseMessage response = await client.GetAsync(address);
            JToken statuses = null;
            if (response.IsSuccessStatusCode)
            {
                statuses = await response.Content.ReadAsAsync<JToken>();
                return statuses;
            }
            return statuses;
        }

    }
}
