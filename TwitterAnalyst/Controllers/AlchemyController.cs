﻿using DomainModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TwitterAnalyst.Controllers
{
    public class AlchemyController : ApiController
    {
        [HttpGet]
        public AlchemySentiment GetTextSentiment(string text)
        {
            AlchemySentiment sentiment =  AlchemyHelper.GetSentimentFromText(text);
            return sentiment; 
        }

        [HttpGet]
        public AlchemyTaxonomy GetTextTaxonomy(string text)
        {
            AlchemyTaxonomy taxonomy = AlchemyHelper.GetTaxonomyFromText(text);
            return taxonomy; 
        }

        [HttpGet]
        public AlchemyConcept GetTextConcepts(string text)
        {
            AlchemyConcept concept = AlchemyHelper.GetConceptsFromText(text);
            return concept;
        }

        [HttpGet]
        public AlchemyEntity GetTextEntities(string text)
        {
            AlchemyEntity entities = AlchemyHelper.GetEntitiesFromText(text);
            return entities;
        }

        [HttpGet]
        public AlchemyKeyword GetTextKeywords(string text)
        {
            AlchemyKeyword keywords = AlchemyHelper.GetKeyWordFromText(text);
            return keywords;
        }

        [HttpGet]
        public AlchemyEmotion GetTextEmotions(string text)
        {
            AlchemyEmotion emotion = AlchemyHelper.GetEmotionFromText(text);
            return emotion;
        }

    }
}
