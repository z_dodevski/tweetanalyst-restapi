﻿using DomainModels.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace TwitterAnalyst
{
    public class YandexHelper
    {
        private static string _yandexapi = "trnsl.1.1.20160106T205041Z.332ee2c135e84f71.fcb402529071f933c6e3f9c236a1dd76865420ce";
        private static string _yandexapi2 = "trnsl.1.1.20160305T164450Z.89e1bba9f7e587b9.f90ff5a68eaa2119a6c8701b36de92e2d3178897";

        public static string Translate(string text, string languageFrom, string languageTo)
        {
            string url = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=" + _yandexapi2
                + "&text=" + text.Replace("#", "")
                + "&lang=" + languageFrom + "-" + languageTo;

            WebClient wc = new WebClient();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            try
            {
               
                wc.Encoding = Encoding.UTF8;
                string rawJson = wc.DownloadString(url);
                TranslationObject translation = JsonConvert.DeserializeObject<TranslationObject>(rawJson);


                return translation.text.First();
            }
            catch (Exception err)
            {
                return string.Empty;
            }


        }

        public static string TranslatePost(string text, string languageFrom, string languageTo)
        {
            string URI = "https://translate.yandex.net/api/v1.5/tr.json/translate";

            string myParameters = "key=" + _yandexapi2;
            myParameters += "&text=" + text.Replace("#", "");
            myParameters += "&lang=" + languageFrom + "-" + languageTo;
            string rawJson = string.Empty; 

            using (WebClient wc = new WebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                wc.Encoding = Encoding.UTF8;
                rawJson = wc.UploadString(URI, myParameters);                       
            }

            JavaScriptSerializer ser = new JavaScriptSerializer();
            try
            {
                TranslationObject translation = JsonConvert.DeserializeObject<TranslationObject>(rawJson);

                return translation.text.First();
            }
            catch (Exception err)
            {
                return string.Empty;
            }

        }

        public static string TranslateLongText(string concatenatedTweets, string from, string to)
        {
            int length = concatenatedTweets.Length;

            int maxWidth = 10000;
            int processedText = 0;
            string translation = string.Empty;


            try
            {
                while (processedText < length)
                {
                    if (processedText + maxWidth >= length)
                        maxWidth = length - processedText;

                    string subString = concatenatedTweets.Substring(processedText, maxWidth);
                    string tweetPhrasesTranslated = YandexHelper.TranslatePost(subString, from, to);

                    translation += tweetPhrasesTranslated;
                    processedText += maxWidth;
                }
            }
            catch(Exception err)
            {
                string error = err.Message; 
            }

            return translation;
        }
    }
}