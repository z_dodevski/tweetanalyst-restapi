﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace TwitterAnalyst.OAuth
{
    /// <summary>
    /// Basic DelegatingHandler that creates an OAuth authorization header based on the OAuthBase
    /// class downloaded from http://oauth.net
    /// </summary>
    public class OAuthMessageHandler : DelegatingHandler
    {
        // Obtain these values by creating a Twitter app at http://dev.twitter.com/
        private static string _consumerKey = "NWo6ABvTwmIdSXFff6FjUMdiu";
        private static string _consumerSecret = "Y5h2t2MpSMv6XZILHylFXxOgikD64JSVMZARJpqYHhJh4FwlVM";
        private static string _token = "4717593273-KnWpUKEUbh1C8OiWyLo9SJ4aVXJDPYTEY7ER3IV";
        private static string _tokenSecret = "QhHlrVmMCJbVWtdWHMBzx14Uj79KTPPBKLw2ECnrJOxPX";
        private static string _yandexapi = "trnsl.1.1.20160106T205041Z.332ee2c135e84f71.fcb402529071f933c6e3f9c236a1dd76865420ce";
        private static string _dandelionApiID = "1bae902d";
        private static string _dandelionAppKey = "5a6dd98589402e4a2c85f4e93093f4a9";


        private OAuthBase _oAuthBase = new OAuthBase();

        public OAuthMessageHandler(HttpMessageHandler innerHandler)
            : base(innerHandler)
        {
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            // Compute OAuth header 
            string normalizedUri;
            string normalizedParameters;
            string authHeader;

            string signature = _oAuthBase.GenerateSignature(
                request.RequestUri,
                _consumerKey,
                _consumerSecret,
                _token,
                _tokenSecret,
                request.Method.Method,
                _oAuthBase.GenerateTimeStamp(),
                _oAuthBase.GenerateNonce(),
                out normalizedUri,
                out normalizedParameters,
                out authHeader);

            request.Headers.Authorization = new AuthenticationHeaderValue("OAuth", authHeader);
            return base.SendAsync(request, cancellationToken);
        }
    }
}