﻿using DomainModels.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace TwitterAnalyst
{
    public class ReportHelper
    {
        public static ColumnChartModel FillColumnChartModel(List<MontlyReportModel> monthlyReportModels)
        {
            ColumnChartModel model = new ColumnChartModel();

            CategoriesModel categories = new CategoriesModel();
            categories.category.Add(new CategoryModel() { label = "Jan" });
            categories.category.Add(new CategoryModel() { label = "Feb" });
            categories.category.Add(new CategoryModel() { label = "Mar" });
            categories.category.Add(new CategoryModel() { label = "Apr" });
            categories.category.Add(new CategoryModel() { label = "May" });
            categories.category.Add(new CategoryModel() { label = "Jun" });
            categories.category.Add(new CategoryModel() { label = "Jul" });
            categories.category.Add(new CategoryModel() { label = "Aug" });
            categories.category.Add(new CategoryModel() { label = "Sep" });
            categories.category.Add(new CategoryModel() { label = "Oct" });
            categories.category.Add(new CategoryModel() { label = "Nov" });
            categories.category.Add(new CategoryModel() { label = "Dec" });
            model.categories.Add(categories);

            ChartDescriptionModel chart = new ChartDescriptionModel();
            chart.caption = "Sentiment of statuses per month";
            chart.subcaption = "";
            chart.theme = "fint";
            chart.xaxisname = "Month";
            chart.yaxisname = "Negative Neutral Positve";
            chart.numberprefix = string.Empty;

            chart.yAxisMinValue = "-1";
            chart.yAxisMaxValue = "1"; 

            model.chart = chart;


            ColumnChartDataSetModel dataset = new ColumnChartDataSetModel();
            dataset.renderas = "area";
            dataset.seriesname = "Sentiment";
            dataset.showvalues = "0";

            foreach (var monthlyModel in monthlyReportModels)
            {
                string value = 0.ToString();

                if (!string.IsNullOrWhiteSpace(monthlyModel.text))
                {
                    AlchemySentiment alchemySentiment = AlchemyHelper.GetSentimentFromTextPost(monthlyModel.text);

                    if (alchemySentiment.docSentiment != null && alchemySentiment.docSentiment.score.HasValue)
                        value = alchemySentiment.docSentiment.score.Value.ToString();
                }
                dataset.data.Add(new ColumnChartDataModel() { value = value });
            }
            model.dataset.Add(dataset);

            return model;
        }

        public static ColumnChartModel FillEmotionsChartModel(List<MontlyReportModel> monthlyReportModels)
        {
            ColumnChartModel model = new ColumnChartModel();

            CategoriesModel categories = new CategoriesModel();
            categories.category.Add(new CategoryModel() { label = "Jan" });
            categories.category.Add(new CategoryModel() { label = "Feb" });
            categories.category.Add(new CategoryModel() { label = "Mar" });
            categories.category.Add(new CategoryModel() { label = "Apr" });
            categories.category.Add(new CategoryModel() { label = "May" });
            categories.category.Add(new CategoryModel() { label = "Jun" });
            categories.category.Add(new CategoryModel() { label = "Jul" });
            categories.category.Add(new CategoryModel() { label = "Aug" });
            categories.category.Add(new CategoryModel() { label = "Sep" });
            categories.category.Add(new CategoryModel() { label = "Oct" });
            categories.category.Add(new CategoryModel() { label = "Nov" });
            categories.category.Add(new CategoryModel() { label = "Dec" });
            model.categories.Add(categories);

            ChartDescriptionModel chart = new ChartDescriptionModel();
            chart.caption = "Emotions of statuses on a monthly basis";
            chart.subcaption = "";
            chart.theme = "fint";
            chart.xaxisname = "Month";
            chart.yaxisname = "Emotion";
            chart.numberprefix = string.Empty;
            model.chart = chart;

            ColumnChartDataSetModel datasetAnger = new ColumnChartDataSetModel();
            datasetAnger.renderas = "area";
            datasetAnger.seriesname = "Anger";
            datasetAnger.showvalues = "0";

            ColumnChartDataSetModel datasetDisgust = new ColumnChartDataSetModel();
            datasetDisgust.renderas = "area";
            datasetDisgust.seriesname = "Disgust";
            datasetDisgust.showvalues = "0";

            ColumnChartDataSetModel datasetFear = new ColumnChartDataSetModel();
            datasetFear.renderas = "area";
            datasetFear.seriesname = "Fear";
            datasetFear.showvalues = "0";

            ColumnChartDataSetModel datasetJoy = new ColumnChartDataSetModel();
            datasetJoy.renderas = "area";
            datasetJoy.seriesname = "Joy";
            datasetJoy.showvalues = "0";

            ColumnChartDataSetModel datasetSadness = new ColumnChartDataSetModel();
            datasetSadness.renderas = "area";
            datasetSadness.seriesname = "Sadness";
            datasetSadness.showvalues = "0";


            try
            {
                foreach (var monthlyModel in monthlyReportModels)
                {
                    AlchemyEmotion alchemySentiment = null;

                    if (!string.IsNullOrWhiteSpace(monthlyModel.text))
                        alchemySentiment = AlchemyHelper.GetEmotionFromTextPost(monthlyModel.text);

                    string value = 0.ToString();
                    datasetAnger.data.Add(new ColumnChartDataModel() { value = alchemySentiment != null ? alchemySentiment.docEmotions.anger.ToString() : value });
                    datasetDisgust.data.Add(new ColumnChartDataModel() { value = alchemySentiment != null ? alchemySentiment.docEmotions.disgust.ToString() : value });
                    datasetFear.data.Add(new ColumnChartDataModel() { value = alchemySentiment != null ? alchemySentiment.docEmotions.fear.ToString() : value });
                    datasetJoy.data.Add(new ColumnChartDataModel() { value = alchemySentiment != null ? alchemySentiment.docEmotions.joy.ToString() : value });
                    datasetSadness.data.Add(new ColumnChartDataModel() { value = alchemySentiment != null ? alchemySentiment.docEmotions.sadness.ToString() : value });

                }
            }
            catch(Exception err)
            {
                string message = err.Message;
            }

            model.dataset.Add(datasetAnger);
            model.dataset.Add(datasetDisgust);
            model.dataset.Add(datasetFear);
            model.dataset.Add(datasetJoy);
            model.dataset.Add(datasetSadness);

            return model;
        }

        public static ColumnChartModel DailyTweets(List<Tweet> tweets, int year)
        {
            ColumnChartModel model = new ColumnChartModel();

            CategoriesModel categories = new CategoriesModel();

            categories.category.Add(new CategoryModel() { label = "Monday" });
            categories.category.Add(new CategoryModel() { label = "Tuesday" });
            categories.category.Add(new CategoryModel() { label = "Wednesday" });
            categories.category.Add(new CategoryModel() { label = "Thursday" });
            categories.category.Add(new CategoryModel() { label = "Friday" });
            categories.category.Add(new CategoryModel() { label = "Saturday" });
            categories.category.Add(new CategoryModel() { label = "Sunday" });

            model.categories.Add(categories);

            ChartDescriptionModel chart = new ChartDescriptionModel();
            chart.caption = "Tweets - day of the week";
            chart.subcaption = "";
            chart.theme = "fint";
            chart.xaxisname = "Month";
            chart.yaxisname = "Percentage";
            chart.numberprefix = string.Empty;

            model.chart = chart;

            List<int> days = new List<int>() { 1, 2, 3, 4, 5, 6, 0 };

            ColumnChartDataSetModel dataset = new ColumnChartDataSetModel();
            dataset.renderas = "";
            dataset.seriesname = "Days";
            dataset.showvalues = "0";

            foreach (int day in days)
            {
                string seriesName = Enum.GetName(typeof(DayOfWeek), day);
                int tweetNumber = tweets.Count(x => x.dateCreated.HasValue && (int)x.dateCreated.Value.DayOfWeek == day);


                dataset.data.Add(new ColumnChartDataModel() { value = tweetNumber.ToString() });
            }
            model.dataset.Add(dataset);
            return model;


        }

        public static PieChartModel FillTaxonomyChartModel(List<MontlyReportModel> monthlyReportModels)
        {
            PieChartModel model = new PieChartModel();


            ChartDescriptionModel chart = new ChartDescriptionModel();
            chart.caption = "Taxonomy of tweets";
            chart.subcaption = "Last Year";
            chart.theme = "fint";
            chart.showlabels = "0";
            chart.showlegend = "1";
            chart.enablemultislicing = "1";
            chart.slicingdistance = "15";
            chart.showpercentvalues = "1";
            chart.showpercentintooltip = "0";
            chart.plottooltext = "Taxonomy : $label User : $datavalue";

            model.chart = chart;

            List<PieChartDataModel> data = new List<PieChartDataModel>();


            try
            {
                foreach (var monthlyModel in monthlyReportModels)
                {
                    AlchemyTaxonomy taxonomy = null;
                    if (!string.IsNullOrWhiteSpace(monthlyModel.text))
                        taxonomy = AlchemyHelper.GetTaxonomyFromTextPost(monthlyModel.text);

                    if (taxonomy != null)
                    {
                        foreach (var category in taxonomy.taxonomy)
                        {
                            string label = category.label.Split('/')[0];
                            if (string.IsNullOrWhiteSpace(label))
                                label = category.label.Split('/')[1];
                            if (string.IsNullOrWhiteSpace(label))
                                label = category.label;


                            if (data.Any(x => x.label == label))
                            {
                                var datasample = data.Where(x => x.label == label).FirstOrDefault();
                                float value = float.Parse(datasample.value);
                                datasample.value = (value + (category.score.HasValue ? category.score.Value : 0)).ToString();
                            }
                            else
                            {
                                data.Add(new PieChartDataModel { label = label, value = category.score.ToString() });
                            }
                        }
                    }

                }
            }
            catch(Exception e)
            {
                string message = e.Message;
            }

            
            model.data = data;
            return model;
        }
    }
}