﻿using DomainModels.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace TwitterAnalyst
{
    public class DandelionHelper
    {
        private static string _dandelionApiID = "c41768be";
        private static string _dandelionAppKey = "141dab5e9e4a1d1527dd592115a5d800";

        public static EntityExtractionObject EntityExtraction(string text)
        {
            EntityExtractionObject entityExtractionObject = new EntityExtractionObject(); 
            string url = "https://api.dandelion.eu/datatxt/nex/v1/?text=" + text +
            "&include=image%2Cabstract%2Ctypes%2Ccategories%2Clod&country=-1&$app_id="
            + _dandelionApiID
            + "&$app_key="
            + _dandelionAppKey;

            WebClient wc = new WebClient();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            try
            {
                wc.Encoding = Encoding.UTF8;
                string jsonString = wc.DownloadString(url);
                entityExtractionObject = JsonConvert.DeserializeObject<EntityExtractionObject>(jsonString);

            }
            catch (Exception err)
            {
                return entityExtractionObject;
            }

            return entityExtractionObject;
        }

        public static SimilarityResult TextSimilarity(string text1, string text2)
        {
            SimilarityResult similarityResult = new SimilarityResult();
            string url = "https://api.dandelion.eu/datatxt/sim/v1/?text1=" + text1 +
            "&text2=" + text2 +
            "&$app_id=" + _dandelionApiID
            + "&$app_key=" + _dandelionAppKey;

            WebClient wc = new WebClient();
            wc.Encoding = Encoding.UTF8;
            JavaScriptSerializer ser = new JavaScriptSerializer();
            try
            {
                string jsonString = wc.DownloadString(url);
                similarityResult = JsonConvert.DeserializeObject<SimilarityResult>(jsonString);

                return similarityResult;
            }
            catch (Exception err)
            {
                return similarityResult;
            }
        }

        public static SimilarityResult TextSimilarityPost(string text1, string text2)
        {
            SimilarityResult similarityResult = new SimilarityResult();
            string url = "https://api.dandelion.eu/datatxt/sim/v1/";
                
            string urlParameters ="text1=" + text1 +
            "&text2=" + text2 +
            "&$app_id=" + _dandelionApiID
            + "&$app_key=" + _dandelionAppKey;

            WebClient wc = new WebClient();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            try
            {
                wc.Encoding = Encoding.UTF8;
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                string rawJson = wc.UploadString(url, urlParameters);
                similarityResult = JsonConvert.DeserializeObject<SimilarityResult>(rawJson);

                return similarityResult;
            }
            catch (Exception err)
            {
                return similarityResult;
            }



        }

        public static SentimentResult DefineSentiment(string text)
        {
            SentimentResult sentiment = new SentimentResult();
            string url = "https://api.dandelion.eu/datatxt/sent/v1/?lang=en&text=" + text +
            "&$app_id=" + _dandelionApiID + "&$app_key=" + _dandelionAppKey;

            WebClient wc = new WebClient();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            try
            {
                wc.Encoding = Encoding.UTF8;
                string jsonString = wc.DownloadString(url);
                sentiment = JsonConvert.DeserializeObject<SentimentResult>(jsonString);

            }
            catch (Exception err)
            {
                return sentiment;
            }
            return sentiment;
        }

        public static TextClassificationResult TextClassification(string text)
        {
            TextClassificationResult result = new TextClassificationResult();
            string url = "https://api.dandelion.eu/datatxt/cl/v1/?text=" + text +
            "&model=648b9f89-b869-4639-9386-5493bfb7a84d&min_score=0.2&$app_id=" + _dandelionApiID + "&$app_key=" + _dandelionAppKey;

            WebClient wc = new WebClient();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            try
            {
                wc.Encoding = Encoding.UTF8;
                string jsonString = wc.DownloadString(url);
                result = JsonConvert.DeserializeObject<TextClassificationResult>(jsonString);

            }
            catch (Exception err)
            {
                return result;
            }
            return result;
        }

        public static ClassificationChartData CreateClassificationChart(string text)
        {
            TextClassificationResult result = TextClassification(text);
            ClassificationChartData chartData = new ClassificationChartData();


            foreach (var label in chartData.labels)
            {
                ClassificationCategories category = result.categories.Where(x=>x.name == label).SingleOrDefault();
                if (category != null)
                    chartData.dataSet.Add((category.score * 100).ToString());
                else
                    chartData.dataSet.Add("0");
            }

            return chartData;

        }

        public static List<TweetSimilarity> CompareTweets(List<Tweet> firstUserTweets, List<Tweet> secondUserTweets)
        {
            List<TweetSimilarity> TweetSimilarity = new List<TweetSimilarity>();

            foreach (var firstTweet in firstUserTweets)
            {
                foreach (var secondTweet in secondUserTweets)
                {
                    SimilarityResult similarityResult = new SimilarityResult(); 
                    string translatedFirstTweet = YandexHelper.Translate(firstTweet.text, "mk", "en");
                    string translatedSecondTweet = YandexHelper.Translate(secondTweet.text, "mk", "en");

                    similarityResult = TextSimilarity(translatedFirstTweet, translatedSecondTweet);

                    TweetSimilarity.Add(new TweetSimilarity { firstTweet = firstTweet, secondTweet = secondTweet, similarityResult = similarityResult });
                }
            }
           
            return TweetSimilarity;
        }
    }
}