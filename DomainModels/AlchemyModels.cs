﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModels;

namespace DomainModels.Models
{
    public class AlchemySentiment
    {
        public string status { get; set; }
        public TextSentiment docSentiment { get; set; }
    }

    public class TextSentiment
    {
        public string type { get; set; }
        public float? score { get; set; }
        public string mixed { get; set; }
    }

    public class AlchemyTaxonomy
    {
        public string status { get; set; }
        public List<TextTaxonomy> taxonomy { get; set; }

        public List<string> categories { get; set; }
        public List<float> presence { get; set; }

        public AlchemyTaxonomy()
        {
            this.categories = new List<string>();
            this.taxonomy = new List<TextTaxonomy>();
            this.presence = new List<float>(); 
        }
    }

    public class TextTaxonomy
    {
        public string label { get; set; }
        public float? score { get; set; }
        public string confident { get; set; }
    }

    public class AlchemyConcept
    {
        public string status { get; set; }
        public string text { get; set; }
        public List<TextConcept> concepts { get; set; }
    }

    public class TextConcept
    {
        public string text { get; set; }
        public float? relevance { get; set; }
        public string website { get; set; }
        public string geo { get; set; }
        public string dbpedia { get; set; }
        public string yago { get; set; }
        public string opencyc { get; set; }
        public string freebase { get; set; }

        public string ciaFactbook { get; set; }
        public string census { get; set; }
        public string geonames { get; set; }
        public string musicBrainz { get; set; }
        public string crunchbase { get; set; }
    }

    public class AlchemyEntity
    {
        public string status { get; set; }
        public string text { get; set; }
        public List<TextEntity> entities { get; set; }
    }

    public class TextEntity
    {
        public string type { get; set; }
        public float? relevance { get; set; }
        public string count { get; set; }
        public string text { get; set; }

        public DisambiguatedObject disambiguated { get; set; }
        public TextSentiment sentiment { get; set; }
    }

    public class DisambiguatedObject
    {
        public string name { get; set; }
        public List<string> subType { get; set; }
        public string website { get; set; }
        public string geo { get; set; }
        public string dbpedia { get; set; }
        public string yago { get; set; }
        public string opencyc { get; set; }
        public string umbel { get; set; }
        public string freebase { get; set; }

        public string ciaFactbook { get; set; }
        public string census { get; set; }
        public string geonames { get; set; }
        public string musicBrainz { get; set; }
        public string crunchbase { get; set; }

    }

    public class AlchemyKeyword
    {
        public string status { get; set; }
        public string text { get; set; }
        public List<TextKeyword> keywords { get; set; }
    }

    public class TextKeyword
    {
        public string text { get; set; }
        public float? relevance { get; set; }
        public TextSentiment sentiment { get; set; }
    }

    public class AlchemyEmotion
    {
        public string status { get; set; }
        public string text { get; set; }
        public TextEmotion docEmotions { get; set; }
        public List<float> emotions { get; set; }

        public AlchemyEmotion()
        {
            this.emotions = new List<float>();
        }
    }

    public class TextEmotion
    {
        public float anger { get; set; }
        public float disgust { get; set; }
        public float fear { get; set; }
        public float joy { get; set; }
        public float sadness { get; set; }
    }
}
