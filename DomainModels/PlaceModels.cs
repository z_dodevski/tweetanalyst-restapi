﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainModels.Models
{
    public class Place
    {
        public string id { get; set; }
        public string name { get; set; }

        public PlaceAttribute attributes { get; set; }
        public BoundingBox bounding_box { get; set; }
        public string country { get; set; }
        public string country_code { get; set; }
        public string full_name { get; set; }
        public string place_type { get; set; }
    }

    public class PlaceAttribute
    {
        public string street_address { get; set; }
        public string postal_code { get; set; }
    }

    public class BoundingBox
    {
        public List<List<List<float>>> coordinates { get; set; }
        public string type { get; set; }

        public BoundingBox()
        {
            this.coordinates = new List<List<List<float>>>();
        }
    }
}