﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainModels.Models
{
    public class ReportModel
    {
        public List<AlchemyConcept> alchemyConcepts { get; set; }
        public List<AlchemyEmotion> alchemyEmotions { get; set; }
        public List<AlchemyEntity> alchemyEntities { get; set; }
        public List<AlchemyKeyword> alchemyKeywords { get; set; }
        public List<AlchemySentiment> alchemySentiments { get; set; }
        public List<AlchemyTaxonomy> alchemyTaxonomys { get; set; }

        public ReportModel()
        {
            this.alchemyConcepts = new List<AlchemyConcept>();
            this.alchemyEmotions = new List<AlchemyEmotion>();
            this.alchemyEntities = new List<AlchemyEntity>();
            this.alchemyKeywords = new List<AlchemyKeyword>();
            this.alchemySentiments = new List<AlchemySentiment>();
            this.alchemyTaxonomys = new List<AlchemyTaxonomy>();
        }
    }

    public class ReportTextModel
    {
        public int year { get; set; }
        public ColumnChartModel columnChartModel { get; set; }
        public ColumnChartModel emotionsChartModel { get; set; }
        public ColumnChartModel dailyTweetsChartModel { get; set; }
        public PieChartModel taxonomyChartModel { get; set; }

        public AlchemyConcept alchemyConcepts { get; set; }
        public AlchemyEmotion alchemyEmotions { get; set; }
        public AlchemyEntity alchemyEntities { get; set; }
        public AlchemyKeyword alchemyKeywords { get; set; }
        public AlchemySentiment alchemySentiments { get; set; }
        public AlchemyTaxonomy alchemyTaxonomys { get; set; }
    }

    public class ColumnChartModel
    {
        public List<CategoriesModel> categories { get; set; }
        public ChartDescriptionModel chart { get; set; }
        public List<ColumnChartDataSetModel> dataset { get; set; }

        public ColumnChartModel()
        {
            this.categories = new List<CategoriesModel>();
            this.dataset = new List<ColumnChartDataSetModel>();
        }
    }

    public class CategoriesModel
    {
        public List<CategoryModel> category { get; set; }

        public CategoriesModel()
        {
            this.category = new List<CategoryModel>();
        }
    }

    public class CategoryModel
    {
        public string label { get; set; }
    }

    public class ChartDescriptionModel
    {
        public string caption { get; set; }
        public string subcaption { get; set; }
        public string theme { get; set; }

        public string numberprefix { get; set; }
        public string xaxisname { get; set; }
        public string yaxisname { get; set; }

        public string startingangle { get; set; }
        public string showlabels { get; set; }
        public string showlegend { get; set; }
        public string enablemultislicing { get; set; }
        public string slicingdistance { get; set; }
        public string showpercentvalues { get; set; }
        public string showpercentintooltip { get; set; }
        public string plottooltext { get; set; }
         
        public string yAxisMaxValue {get; set;}
        public string yAxisMinValue {get; set;}        

        public ChartDescriptionModel()
        {
            this.numberprefix = string.Empty;
            this.xaxisname = string.Empty;
            this.yaxisname = string.Empty;

            this.startingangle = string.Empty;
            this.showlabels = string.Empty;
            this.showlegend = string.Empty;
            this.enablemultislicing = string.Empty;
            this.slicingdistance = string.Empty;
            this.showpercentvalues = string.Empty;
            this.showpercentintooltip = string.Empty;
            this.plottooltext = string.Empty;
            this.yAxisMaxValue = string.Empty;
            this.yAxisMinValue = string.Empty;
        }
    }

    public class ColumnChartDataSetModel
    {
        public string seriesname { get; set; }
        public string renderas { get; set; }
        public string showvalues { get; set; }
        public List<ColumnChartDataModel> data { get; set; }

        public ColumnChartDataSetModel()
        {
            this.data = new List<ColumnChartDataModel>();
        }
    }

    public class ColumnChartDataModel
    {
        public string value { get; set; }
    }

    public class MontlyReportModel
    {
        public int month { get; set; }
        public int year { get; set; }
        public string text { get; set; }
    }

    public class PieChartModel
    {
        public ChartDescriptionModel chart { get; set; }
        public List<PieChartDataModel> data { get; set; }

        public PieChartModel()
        {
            this.data = new List<PieChartDataModel>(); 
        }

    }

    public class PieChartDataModel
    {
        public string label { get; set; }
        public string value { get; set; }
    }
}