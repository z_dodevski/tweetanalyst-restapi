﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainModels.Models
{
    public class StatusResponse
    {
        public List<Status> statuses { get; set; }
    }

    public class Status
    {
        public string text { get; set; }
    }

    public class TranslationObject
    {
        public List<string> text { get; set; }
    }

    public class EntityExtractionObject
    {
        public List<Annotation> annotations { get; set; }
    }

    public class Annotation
    {
        public string @abstract { get; set; }
        public string spot { get; set; }
        public string uri { get; set; }
        public string label { get; set; }
        public double confidence { get; set; }
        public Image image { get; set; }
        public LOD lod { get; set; }
    }

    public class Image
    {
        public string full { get; set; }
        public string thumbnail { get; set; }
    }

    public class LOD
    {
        public string dbpedia { get; set; }
        public string wikipedia { get; set; }
    }

    public class SimilarityResult
    {
        public float similarity { get; set; }
    }

    public class SentimentResult
    {
        public Sentiment sentiment { get; set; }
    }

    public class Sentiment
    {
        public string score { get; set; }
        public string type { get; set; }
    }

    public class TextClassificationResult
    {
        public List<ClassificationCategories> categories { get; set; }
    }

    public class ClassificationCategories
    {
        public string name { get; set; }
        public float score { get; set; }
        public List<ScoreDetails> scoreDetails { get; set; }
    }

    public class ScoreDetails
    {
        public string entity { get; set; }
        public string weight { get; set; }
    }
}