﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainModels.Models
{
    public class Tweet
    {
        public long? id { get; set; }
        public string id_str { get; set; }

        public string text { get; set; }
        public Entities entities { get; set; }
        public User user { get; set; }

        public List<Coordinates> coordinates { get; set; }
        public string created_at { get; set; }
        public DateTime? dateCreated { get; set; }
        public int? favorite_count { get; set; }
        public string lang { get; set; }
        public Place place { get; set; }
        public int? retweet_count { get; set; }

        public string translatedText { get; set; }
        public EntityExtractionObject extractedEntities { get; set; }
        public SentimentResult sentimentResult { get; set; }
        public ClassificationChartData classificationChartData { get; set; }
        public TextClassificationResult classificationResult { get; set; }

        public AlchemyConcept alchemyConcept { get; set; }
        public AlchemyEmotion alchemyEmotion { get; set; }
        public AlchemyEntity alchemyEntity  { get; set; }
        public AlchemyKeyword alchemyKeyword { get; set; }
        public AlchemySentiment alchemySentiment { get; set; }
        public AlchemyTaxonomy alchemyTaxonomy { get; set; }
    }

    public class BoundingBoxCoordinates
    {
        List<Coordinates> coordinates { get; set; }

        public BoundingBoxCoordinates()
        {
            this.coordinates = new List<Coordinates>();
        }
    }

    public class Coordinates
    {
        public List<float> coordinates { get; set; }
        //public string type { get; set; }

        public Coordinates()
        {
            this.coordinates = new List<float>();
        }
    }

    public class ClassificationChartData
    {
        public List<string> labels { get; set; }
        public List<string> dataSet { get; set; }

        public ClassificationChartData()
        {
            labels = new List<string> {"environmental issue", "weather", "health", "sport", "education", "culture, entertainment, leisure, gossip", "religion and belief", "social issues, unrest, conflicts, war", "crime, law, disaster and accident", "politics", "economy, business, finance, labour", "science and technology"};
            dataSet = new List<string>();
        }
    }
}