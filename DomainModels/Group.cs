﻿using DomainModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModels
{
    public class UserGroup
    {
        public List<User> users { get; set; }


        public int? favourites_count { get; set; }
        public int? followers_count { get; set; }
        public int? friends_count { get; set; }
        public string statuses_count { get; set; }

        public List<Entities> entities { get; set; }

        public List<Tweet> tweets { get; set; }
        public List<UserMentions> userMentions { get; set; }
        public List<Hashtags> hashTags { get; set; }
        public List<UserMentions> retweeters { get; set; }
        public List<Tweet> mostLikedTweets { get; set; }
        public List<Tweet> statuses { get; set; }

        public ReportTextModel report { get; set; }
    }
}
