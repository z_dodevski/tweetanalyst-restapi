﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainModels.Models
{
    public class User
    {
        public long id { get; set; }
        public string id_str { get; set; }
 

        public string description { get; set; }
        public List<Entities> entities { get; set; }
        public int? favourites_count { get; set; }
        public int? followers_count { get; set; }
        public int? friends_count { get; set; }
        public string location { get; set; }
        public string name { get; set; }
        public string profile_background_image_url { get; set; }
        public string profile_image_url { get; set; }
        public string screen_name { get; set; }
        public string statuses_count { get; set; }

        public Tweet status { get; set; }
        public List<Tweet> tweets { get; set; }
        public List<UserMentions> userMentions {get; set;}
        public List<Hashtags> hashTags { get; set; }
        public List<UserMentions> retweeters { get; set; }
        public List<Tweet> mostLikedTweets { get; set; }

        public User()
        {
            this.tweets = new List<Tweet>();
            this.userMentions = new List<UserMentions>();
            this.hashTags = new List<Hashtags>();
            this.retweeters = new List<UserMentions>();
            this.mostLikedTweets = new List<Tweet>(); 
        }
    }

    public class UserIDs
    {
        public List<string> ids { get; set; }
    }

    public class UserComparison
    {
        public User firstUser {get; set; }
        public User secondUser {get; set;}

        public List<Hashtags> commonHashtags {get; set;}
        public List<UserMentions> commonUserMentions {get; set;}

        public TweetSimilarity mostSimilarTweet {get; set;}

        public List<TweetSimilarity> tweetSimilarity {get; set;}
    }

    public class TweetSimilarity
    {
        public Tweet firstTweet { get; set; }
        public Tweet secondTweet { get; set; }
        public SimilarityResult similarityResult { get; set; }
    }

    public class TweetsSimilarity
    {
        public User firstUser { get; set; }
        public User secondUser { get; set; }
        public SimilarityResult similarityResult { get; set; }
        public List<TextConcept> concepts { get; set; }
        public List<TextEntity> entities { get; set; }

        public List<string> hashtags { get; set; }
        public List<string> mentionedUsers { get; set; }

    }
}