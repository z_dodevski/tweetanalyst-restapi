﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainModels.Models
{
    public class Entities
    {
        public List<Hashtags> hashtags { get; set; }
        public List<Url> urls { get; set; }
        public List<Media> media { get; set; }
        public List<UserMentions> user_mentions { get; set; }

        public Entities()
        {
            this.hashtags = new List<Hashtags>();
            this.urls = new List<Url>();
            this.media = new List<Media>();
            this.user_mentions = new List<UserMentions>();
        }
    }

    public class Hashtags
    {
        public string text { get; set; }

        public Hashtags()
        {

        }

        public Hashtags(string text)
        {
            this.text = text; 
        }
    }

    public class Media
    {
        public string display_url { get; set; }
        public string url { get; set; }
        public long? id { get; set; }
        public string type { get; set; }
        public string media_url { get; set; }
    }

    public class Url
    {
        public string url { get; set; }
        public string display_url { get; set; }
        public string expanded_url { get; set; }
    }

    public class UserMentions
    {
        public string name { get; set; }
        public string screen_name { get; set; }
        public long id { get; set; }
        public string id_str { get; set; }
    }
}